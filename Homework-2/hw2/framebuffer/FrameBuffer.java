/*

*/

package framebuffer;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

/**
    A {@code FrameBuffer} represents a two-dimensional array of pixel data.
    The pixel data is stored as a one dimensional array in row-major order.
    The first row of data should be displayed as the top row of pixels
    in the image.
<p>
    A "viewport" is a two-dimensional sub array of a {@code FrameBuffer}.
    A {@code FrameBuffer} has one current viewport. The current viewport
    is represented by its upper-left-hand corner and its lower-right-hand
    corner.
<p>
    {@code FrameBuffer} and viewport coordinates act like Java
    {@link java.awt.Graphics2D} coordinates; the positive x direction is
    to the right and the positive y direction is downward.
*/
public class FrameBuffer
{
   private Color bgColorFB;      // the framebuffer's background color
   private int width;            // the framebuffer's width
   private int height;           // the framebuffer's height
   public  int[] pixel_buffer;   // contains each pixel's color data for a rendered frame

   // Coordinates of the current viewport within the framebuffer.
   private int vp_ul_x;        // upper-left-hand corner
   private int vp_ul_y;
   private int vp_lr_x;        // lower-right-hand corner
   private int vp_lr_y;
   private Color bgColorVP;    // the viewport's background color

   /**
      Construct a {@code FrameBuffer} with the given dimensions.
      Initialize the framebuffer to hold all black pixels.

      @param w  width of the {@code FrameBuffer}.
      @param h  height of the {@code FrameBuffer}.
   */
   public FrameBuffer(int w, int h)
   {
      this(w, h, Color.black);
   }


   /**
      Construct a {@code FrameBuffer} with the given dimensions and
      initialize the {@code FrameBuffer} to the given {@link Color}.

      @param w  width of the {@code FrameBuffer}.
      @param h  height of the {@code FrameBuffer}.
      @param c  background {@link Color} for the {@code FrameBuffer}
   */
   public FrameBuffer(int w, int h, Color c)
   {
      this.width  = w;
      this.height = h;

      // Set the default viewport (to be the whole framebuffer).
      this.vp_ul_x = 0;
      this.vp_ul_y = 0;
      this.vp_lr_x = this.width - 1;
      this.vp_lr_y = this.height - 1;

      // Create the pixel buffer.
      pixel_buffer = new int[this.width * this.height];

      // Initialize the pixel buffer.
      this.bgColorFB = c;
      this.bgColorVP = c;
      clearFB();
   }


   /**
      Construct a {@code FrameBuffer} from a PPM image file.
      <p>
      This can be used to initialize a {@code FrameBuffer}
      with a background image.

      @param inputFileName  must name a PPM image file with magic number P6.
   */
   public FrameBuffer(String inputFileName)
   {
      // Read the pixel data in a PPM file.
      // http://stackoverflow.com/questions/2693631/read-ppm-file-and-store-it-in-an-array-coded-with-c
      try
      {
         FileInputStream fis = new FileInputStream(inputFileName);

         // Read image format string "P6".
         String magicNumber = "";
         char c = (char)fis.read();
         while (c != '\n')
         {
            magicNumber += c;
            c = (char)fis.read();
         }
         if (! magicNumber.trim().startsWith("P6"))
         {
            System.err.printf("ERROR! Improper PPM number in file %s\n", inputFileName);
            System.exit(-1);
         }

         c = (char)fis.read();
         if ( '#' == c ) // read (and discard) IrfanView comment
         {
            while (c != '\n')
            {
               c = (char)fis.read();
            }
            c = (char)fis.read();
         }

         // Read image dimensions.
         String widthDim = "";
         while (c != ' ' && c != '\n')
         {
            widthDim += c;
            c = (char)fis.read();
         }

         String heightDim = "";
         c = (char)fis.read();
         while (c != '\n')
         {
            heightDim += c;
            c = (char)fis.read();
         }

         fis.close();

         this.width  = Integer.parseInt(widthDim.trim());
         this.height = Integer.parseInt(heightDim.trim());

         // Set the default viewport (to be the whole framebuffer).
         vp_ul_x = 0;
         vp_ul_y = 0;
         vp_lr_x = this.width - 1;
         vp_lr_y = this.height - 1;

         // Create the pixel buffer.
         pixel_buffer = new int[this.width * this.height];

         // Initialize the pixel buffer.
         this.bgColorFB = Color.black;
         this.bgColorVP = Color.black;
         clearFB();

         setViewport(vp_ul_x, vp_ul_y, inputFileName);
      }
      catch (IOException e)
      {
         System.err.printf("ERROR! Could not read %s\n", inputFileName);
         e.printStackTrace(System.err);
         System.exit(-1);
      }
   }


   /**
      Get the width of the {@code FrameBuffer}.

      @return this {@code FrameBuffer}'s width
   */
   public int getWidthFB()
   {
      return this.width;
   }


   /**
      Get the height of the {@code FrameBuffer}.

      @return this {@code FrameBuffer}'s height
   */
   public int getHeightFB()
   {
      return this.height;
   }


   /**
      Get the background {@link Color} of the {@code FrameBuffer}.

      @return this {@code FrameBuffer}'s background {@link Color}
   */
   public Color getBgColorFB()
   {
      return this.bgColorFB;
   }


   /**
      Set the background {@link Color} of the {@code FrameBuffer}.

      @param c  this {@code FrameBuffer}'s new background {@link Color}
   */
   public void setBgColorFB(Color c)
   {
      this.bgColorFB = c;
   }


   /**
      Clear the {@code FrameBuffer} using its background color.
   */
   public void clearFB()
   {
      clearFB(bgColorFB);
   }


   /**
      Clear the {@code FrameBuffer} using the given {@link Color}.

      @param c  {@link Color} to clear {@code FrameBuffer} with
   */
   public void clearFB(Color c)
   {
      for (int y = 0; y < height; y++)
      {
         for (int x = 0; x < width; x++)
         {
            setPixelFB(x, y, c);
         }
      }
   }


   /**
      Get the {@link Color} of the pixel with coordinates
      (x,y) in the framebuffer.

      @param x  horizontal coordinate within the {@code FrameBuffer}
      @param y  vertical coordinate within the {@code FrameBuffer}
      @return the {@link Color} of the current pixel at the given pixel coordinates
   */
   public Color getPixelFB(int x, int y)
   {
      int index = (y*width + x);
      try
      {
         int rgb = pixel_buffer[index];
         return new Color(rgb);
      }
      catch(ArrayIndexOutOfBoundsException e)
      {
         System.err.println("FrameBuffer: Bad pixel coordinate (" + x + ", " + y +")");
       //e.printStackTrace(System.err);
         return Color.black;
      }
   }


   /**
      Set the {@link Color} of the pixel with coordinates
      (x,y) in the {@code FrameBuffer}.

      @param x  horizontal coordinate within the {@code FrameBuffer}
      @param y  vertical coordinate within the {@code FrameBuffer}
      @param c  {@link Color} for the pixel at the given pixel coordinates
   */
   public void setPixelFB(int x, int y, Color c)
   {
      int index = (y*width + x);
      try
      {
         pixel_buffer[index] = c.getRGB();
      }
      catch(ArrayIndexOutOfBoundsException e)
      {
         System.err.println("FrameBuffer: Bad pixel coordinate (" + x + ", " + y +")");
       //e.printStackTrace(System.err);
      }
   }


   /**
      Set the coordinates, within the {@code FrameBuffer}, of
      the viewport's upper-left-hand corner, width, and height.
      (Using upper-left-hand corner, width, and height is
       like Java's {@link java.awt.Rectangle} class and
       {@link java.awt.Graphics#drawRect} method.)

      @param vp_ul_x  upper left hand x-coordinate of new viewport rectangle
      @param vp_ul_y  upper left hand y-coordinate of new viewport rectangle
      @param width    viewport's width
      @param height   viewport's height
   */
   public void setViewport(int vp_ul_x, int vp_ul_y, int width, int height)
   {
      this.vp_ul_x = vp_ul_x;
      this.vp_ul_y = vp_ul_y;
      this.vp_lr_x = vp_ul_x + width - 1;
      this.vp_lr_y = vp_ul_y + height - 1;
   }


   /**
      Set the viewport to be the whole {@code FrameBuffer},
   */
   public void setViewport()
   {
      this.vp_ul_x = 0;
      this.vp_ul_y = 0;
      this.vp_lr_x = this.width - 1;
      this.vp_lr_y = this.height - 1;
   }


   /**
      Create a viewport from a {@code FrameBuffer}.
      <p>
      The size of the viewport will be the size of the source {@code FrameBuffer}.

      @param vp_ul_x   upper left hand x-coordinate of new viewport rectangle
      @param vp_ul_y   upper left hand y-coordinate of new viewport rectangle
      @param sourceFB  {@code FrameBuffer} to use as the source of the pixel data..
   */
   public void setViewport(int vp_ul_x, int vp_ul_y, FrameBuffer sourceFB)
   {
      int vpWidth  = sourceFB.width;
      int vpHeight = sourceFB.height;

      this.vp_ul_x = vp_ul_x;
      this.vp_ul_y = vp_ul_y;
      this.vp_lr_x = vp_ul_x + vpWidth - 1;
      this.vp_lr_y = vp_ul_y + vpHeight - 1;

      // Read pixel data, one pixel at a time, from the source FrameBuffer.
      for (int y = 0; y < vpHeight; y++)
      {
         for (int x = 0; x < vpWidth; x++)
         {
            this.setPixelVP(x, y, sourceFB.getPixelFB(x,y));
         }
      }
   }


   /**
      Create a viewport from a PPM image file.
      <p>
      The size of the viewport will be the size of the image.
      <p>
      This can be used to initialize a viewport with a background image.

      @param vp_ul_x        upper left hand x-coordinate of new viewport rectangle
      @param vp_ul_y        upper left hand y-coordinate of new viewport rectangle
      @param inputFileName  must name a PPM image file with magic number P6.
   */
   public void setViewport(int vp_ul_x, int vp_ul_y, String inputFileName)
   {
      // Read the pixel data in a PPM file.
      // http://stackoverflow.com/questions/2693631/read-ppm-file-and-store-it-in-an-array-coded-with-c
      try
      {
         FileInputStream fis = new FileInputStream(inputFileName);

         // Read image format string "P6".
         String magicNumber = "";
         char c = (char)fis.read();
         while (c != '\n')
         {
            magicNumber += c;
            c = (char)fis.read();
         }
         if (! magicNumber.trim().startsWith("P6"))
         {
            System.err.printf("ERROR! Improper PPM number in file %s\n", inputFileName);
            System.exit(-1);
         }

         c = (char)fis.read();
         if ( '#' == c ) // read (and discard) IrfanView comment
         {
            while (c != '\n')
            {
               c = (char)fis.read();
            }
            c = (char)fis.read();
         }

         // Read image dimensions.
         String widthDim = "";
         while (c != ' ' && c != '\n')
         {
            widthDim += c;
            c = (char)fis.read();
         }

         String heightDim = "";
         c = (char)fis.read();
         while (c != '\n')
         {
            heightDim += c;
            c = (char)fis.read();
         }

         // Read image rgb dimensions (which we don't use).
         c = (char)fis.read();
         while (c != '\n')
         {
            c = (char)fis.read();
         }

         int vpWidth  = Integer.parseInt(widthDim.trim());
         int vpHeight = Integer.parseInt(heightDim.trim());

         this.vp_ul_x = vp_ul_x;
         this.vp_ul_y = vp_ul_y;
         this.vp_lr_x = vp_ul_x + vpWidth - 1;
         this.vp_lr_y = vp_ul_y + vpHeight - 1;

         // Create a data array.
         byte[] pixelData = new byte[3];
         // Read pixel data, one pixel at a time, from the PPM file.
         for (int y = 0; y < vpHeight; y++)
         {
            for (int x = 0; x < vpWidth; x++)
            {
               if ( fis.read(pixelData, 0, 3) != 3 )
               {
                  System.err.printf("ERROR! Could not load %s\n", inputFileName);
                  System.exit(-1);
               }
               int r = pixelData[0];
               int g = pixelData[1];
               int b = pixelData[2];
               if (r < 0) r = 256+r;  // convert from signed byte to unsigned byte
               if (g < 0) g = 256+g;
               if (b < 0) b = 256+b;
               setPixelVP(x, y, new Color(r, g, b));
            }
         }
         fis.close();
      }
      catch (IOException e)
      {
         System.err.printf("ERROR! Could not read %s\n", inputFileName);
         e.printStackTrace(System.err);
         System.exit(-1);
      }
   }


   /**
      Get the width of the viewport.

      @return width of the current viewport rectangle
   */
   public int getWidthVP()
   {
      return vp_lr_x - vp_ul_x + 1;
   }


   /**
      Get the height of the viewport.

      @return height of the current viewport rectangle
   */
   public int getHeightVP()
   {
      return vp_lr_y - vp_ul_y + 1;
   }


   /**
      Get the upper left hand corner of the viewport.

      @return upper left hand corner of current viewport rectangle
   */
   public java.awt.Point getLocationVP()
   {
      return new java.awt.Point(vp_ul_x, vp_ul_y);
   }


   /**
      Get the background {@link Color} of the viewport.

      @return background {@link Color} of current viewport
   */
   public Color getBgColorVP()
   {
      return this.bgColorVP;
   }


   /**
      Set the background {@link Color} of the viewport.

      @param c  background {@link Color} of current viewport
   */
   public void setBgColorVP(Color c)
   {
      this.bgColorVP = c;
   }


   /**
      Clear the viewport using its background color.
   */
   public void clearVP()
   {
      clearVP(bgColorVP);
   }


   /**
      Clear the viewport using the given {@link Color}.

      @param c  {@link Color} to clear current viewport with
   */
   public void clearVP(Color c)
   {
      int wVP = getWidthVP();
      int hVP = getHeightVP();

      for (int y = 0; y < hVP; y++)
      {
         for (int x = 0; x < wVP; x++)
         {
            setPixelVP(x, y, c);
         }
      }
   }


   /**
      Get the {@link Color} of the pixel with coordinates
      (x,y) relative to the current viewport.

      @param x  horizontal coordinate within the current viewport
      @param y  vertical coordinate within the current viewport
      @return the {@link Color} of the current pixel at the given viewport coordinates
   */
   public Color getPixelVP(int x, int y)
   {
      return getPixelFB(vp_ul_x + x, vp_ul_y + y);
   }


   /**
      Set the {@link Color} of the pixel with coordinates
      (x,y) relative to the current viewport.

      @param x  horizontal coordinate within the current viewport
      @param y  vertical coordinate within the current viewport
      @param c  {@link Color} for the pixel at the given viewport coordinates
   */
   public void setPixelVP(int x, int y, Color c)
   {
      setPixelFB(vp_ul_x + x, vp_ul_y + y, c);
   }


   /**
      Create a new {@code FrameBuffer} containing the pixel data
      from this {@code FrameBuffer}'s current viewport rectangle.

      @return {@code FrameBuffer} object holding pixel data from the current viewport rectangle
   */
   public FrameBuffer convertVP2FB()
   {
      int wVP = this.getWidthVP();
      int hVP = this.getHeightVP();

      FrameBuffer vp_fb = new FrameBuffer( wVP, hVP );
      vp_fb.bgColorFB = this.bgColorVP;

      // Copy the current viewport into the new framebuffer's pixel buffer.
      for (int y = 0; y < hVP; y++)
      {
         for (int x = 0; x < wVP; x++)
         {
            vp_fb.setPixelFB( x, y, this.getPixelVP(x, y) );
         }
      }

      return vp_fb;
   }


   /**
      Create a new {@code FrameBuffer} containing the pixel data
      from just the red plane of this {@code FrameBuffer}.
      <p>
      The new {@code FrameBuffer} will have the same viewport as this
      {@code FrameBuffer}.

      @return {@code FrameBuffer} object holding just red pixel data from this {@code FrameBuffer}
   */
   public FrameBuffer convertRed2FB()
   {
      FrameBuffer red_fb = new FrameBuffer(this.width, this.height);
      red_fb.bgColorFB = this.bgColorFB;
      red_fb.bgColorVP = this.bgColorVP;
      red_fb.vp_ul_x = this.vp_ul_x;
      red_fb.vp_ul_y = this.vp_ul_y;
      red_fb.vp_lr_x = this.vp_lr_x;
      red_fb.vp_lr_y = this.vp_lr_y;

      // Copy the framebuffer's red values into the new framebuffer's pixel buffer.
      for (int y = 0; y < this.height; y++)
      {
         for (int x = 0; x < this.width; x++)
         {
            Color c = new Color(this.getPixelFB(x, y).getRed(), 0, 0);
            red_fb.setPixelFB(x, y, c);
         }
      }

      return red_fb;
   }


   /**
      Create a new {@code FrameBuffer} containing the pixel data
      from just the green plane of this {@code FrameBuffer}.
      <p>
      The new {@code FrameBuffer} will have the same viewport as this
      {@code FrameBuffer}.

      @return {@code FrameBuffer} object holding just green pixel data from this {@code FrameBuffer}
   */
   public FrameBuffer convertGreen2FB()
   {
      FrameBuffer green_fb = new FrameBuffer(this.width, this.height);
      green_fb.bgColorFB = this.bgColorFB;
      green_fb.bgColorVP = this.bgColorVP;
      green_fb.vp_ul_x = this.vp_ul_x;
      green_fb.vp_ul_y = this.vp_ul_y;
      green_fb.vp_lr_x = this.vp_lr_x;
      green_fb.vp_lr_y = this.vp_lr_y;

      // Copy the framebuffer's green values into the new framebuffer's pixel buffer.
      for (int y = 0; y < this.height; y++)
      {
         for (int x = 0; x < this.width; x++)
         {
            Color c = new Color(0, this.getPixelFB(x, y).getGreen(), 0);
            green_fb.setPixelFB(x, y, c);
         }
      }

      return green_fb;
   }


   /**
      Create a new {@code FrameBuffer} containing the pixel data
      from just the blue plane of this {@code FrameBuffer}.
      <p>
      The new {@code FrameBuffer} will have the same viewport as this
      {@code FrameBuffer}.

      @return {@code FrameBuffer} object holding just blue pixel data from this {@code FrameBuffer}
   */
   public FrameBuffer convertBlue2FB()
   {
      FrameBuffer blue_fb = new FrameBuffer(this.width, this.height);
      blue_fb.bgColorFB = this.bgColorFB;
      blue_fb.bgColorVP = this.bgColorVP;
      blue_fb.vp_ul_x = this.vp_ul_x;
      blue_fb.vp_ul_y = this.vp_ul_y;
      blue_fb.vp_lr_x = this.vp_lr_x;
      blue_fb.vp_lr_y = this.vp_lr_y;

      // Copy the framebuffer's blue values into the new framebuffer's pixel buffer.
      for (int y = 0; y < this.height; y++)
      {
         for (int x = 0; x < this.width; x++)
         {
            Color c = new Color(0, 0, this.getPixelFB(x, y).getBlue());
            blue_fb.setPixelFB(x, y, c);
         }
      }

      return blue_fb;
   }


   /**
      Write this {@code FrameBuffer} to the specified PPM file.
      <p>
      <a href="https://en.wikipedia.org/wiki/Netpbm_format" target="_top">
               https://en.wikipedia.org/wiki/Netpbm_format</a>

      @param filename  name of PPM image file to hold {@code FrameBuffer} data
   */
   public void dumpFB2File(String filename)
   {
      dumpPixels2File(0, 0, width-1, height-1, filename);
   }


   /**
      Write the viewport to the specified PPM file.
      <p>
      <a href="https://en.wikipedia.org/wiki/Netpbm_format" target="_top">
               https://en.wikipedia.org/wiki/Netpbm_format</a>

      @param filename  name of PPM image file to hold viewport data
   */
   public void dumpVP2File(String filename)
   {
      dumpPixels2File(vp_ul_x, vp_ul_y, vp_lr_x, vp_lr_y, filename);
   }


   /**
      <p>
      Write a rectangular sub array of pixels from this {@code FrameBuffer}
      to the specified PPM file.
      <p>
      <a href="https://en.wikipedia.org/wiki/Netpbm_format#PPM_example" target="_top">
               https://en.wikipedia.org/wiki/Netpbm_format#PPM_example</a>
      <p>
<a href="http://stackoverflow.com/questions/2693631/read-ppm-file-and-store-it-in-an-array-coded-with-c" target="_top">
         http://stackoverflow.com/questions/2693631/read-ppm-file-and-store-it-in-an-array-coded-with-c</a>

      @param ul_x      upper left hand x-coordinate of pixel data rectangle
      @param ul_y      upper left hand y-coordinate of pixel data rectangle
      @param lr_x      lower right hand x-coordinate of pixel data rectangle
      @param lr_y      lower right hand y-coordinate of pixel data rectangle
      @param filename  name of PPM image file to hold pixel data
   */
   public void dumpPixels2File(int ul_x, int ul_y, int lr_x, int lr_y, String filename)
   {
      int p_width  = lr_x - ul_x + 1;
      int p_height = lr_y - ul_y + 1;

      FileOutputStream fos = null;
      try  // open the file
      {
         fos = new FileOutputStream(filename);
      }
      catch (FileNotFoundException e)
      {
         System.err.printf("ERROR! Could not open file %s\n", filename);
         e.printStackTrace(System.err);
         System.exit(-1);
      }
      //System.err.printf("Created file %s\n", filename);

      try  // write data to the file
      {
         // write the PPM header information first
         fos.write( ("P6\n" + p_width + " " + p_height + "\n" + 255 + "\n").getBytes() );

         // write the pixel data to the file
         byte[] temp = new byte[p_width*3];  // array to hold one row of data
         for (int n = 0; n < p_height; n++)
         {  // write one row of pixels at a time,

            // read from the top row of the data buffer
            // down towards the bottom row
            for (int i = 0; i < temp.length; i+=3)
            {
               int rgb = pixel_buffer[((ul_y+n)*width + ul_x) + i/3];
               Color c = new Color(rgb);
               temp[i + 0] = (byte)(c.getRed());
               temp[i + 1] = (byte)(c.getGreen());
               temp[i + 2] = (byte)(c.getBlue());
            }
            /*
            // read from the bottom row of the data buffer
            // up towards the top row
            for (int i = 0; i < temp.length; i+=3)
            {
               int rgb = pixel_buffer[((lr_y-n)*width + ul_x) + i/3];
               Color c = new Color(rgb);
               temp[i + 0] = (byte)(c.getRed());
               temp[i + 1] = (byte)(c.getGreen());
               temp[i + 2] = (byte)(c.getBlue());
            }
            */
            fos.write(temp); // write one row of data
         }
      }
      catch (IOException e)
      {
         System.err.printf("ERROR! Could not write to file %s\n", filename);
         e.printStackTrace(System.err);
         System.exit(-1);
      }

      try
      {
         fos.close();
      }
      catch (IOException e)
      {
         System.err.printf("ERROR! Could not close file %s\n", filename);
         e.printStackTrace(System.err);
         System.exit(-1);
      }
   }//dumpPixels2File()


   /**
      Write this {@code FrameBuffer} to the specified image file
      using the specified file format.

      @param filename    name of the image file to hold framebuffer data
      @param formatName  informal name of the image format
   */
   public void dumpFB2File(String filename, String formatName)
   {
      dumpPixels2File(0, 0, width-1, height-1, filename, formatName);
   }


   /**
      Write the viewport to the specified image file
      using the specified file format.

      @param filename    name of the image file to hold viewport data
      @param formatName  informal name of the image format
   */
   public void dumpVP2File(String filename, String formatName)
   {
      dumpPixels2File(vp_ul_x, vp_ul_y, vp_lr_x, vp_lr_y, filename, formatName);
   }


   /**
      <p>
      Write a rectangular sub array of pixels from this {@code FrameBuffer}
      to the specified image file using the specified file format.
      <p>
      Use the static method {@link ImageIO#getWriterFormatNames}
      to find out what informal image format names can be used
      (for example, png, gif, jpg, bmp).

      @param ul_x        upper left hand x-coordinate of pixel data rectangle
      @param ul_y        upper left hand y-coordinate of pixel data rectangle
      @param lr_x        lower right hand x-coordinate of pixel data rectangle
      @param lr_y        lower right hand y-coordinate of pixel data rectangle
      @param filename    name of the image file to hold pixel data
      @param formatName  informal name of the image format
   */
   public void dumpPixels2File(int ul_x, int ul_y, int lr_x, int lr_y, String filename, String formatName)
   {
      int p_width  = lr_x - ul_x + 1;
      int p_height = lr_y - ul_y + 1;

      FileOutputStream fos = null;
      try  // open the file
      {
         fos = new FileOutputStream(filename);
      }
      catch (FileNotFoundException e)
      {
         System.err.printf("ERROR! Could not open file %s\n", filename);
         e.printStackTrace(System.err);
         System.exit(-1);
      }
      //System.err.printf("Created file %s\n", filename);

      BufferedImage bi = new BufferedImage(p_width, p_height, BufferedImage.TYPE_INT_RGB);
      for (int n = 0; n < p_height; n++)
      {
         for (int i = 0; i < p_width; i++)
         {
            int rgb = pixel_buffer[((ul_y+n)*width + ul_x) + i];
            bi.setRGB(i, n, rgb);
         }
      }
      try
      {
         ImageIO.write(bi, formatName, fos);
      }
      catch (IOException e)
      {
         System.err.printf("ERROR! Could not write to file %s\n", filename);
         e.printStackTrace(System.err);
         System.exit(-1);
      }

      try
      {
         fos.close();
      }
      catch (IOException e)
      {
         System.err.printf("ERROR! Could not close file %s\n", filename);
         e.printStackTrace(System.err);
         System.exit(-1);
      }
   }//dumpPixels2File()



   /**
      A simple test of the {@code FrameBuffer} class.
      <p>
      It fills the framebuffer with a test pattern.
   */
   public void fbTest()
   {
      for (int y = 0; y < this.height; y++)
      {
         for (int x = 0; x < this.width; x++)
         {
            int gray = (x|y)%255;
            setPixelFB(x, y, new Color(gray, gray, gray));
         }
      }
   }//fbTest()


   /**
      A simple test of the viewport.
      <p>
      It fills the viewport with a test pattern.
   */
   public void vpTest()
   {
      for (int y = 0; y < this.getHeightVP(); y++)
      {
         for (int x = 0; x < this.getWidthVP(); x++)
         {
            int gray = (x|y)%255;
            setPixelVP(x, y, new Color(gray, gray, gray));
         }
      }
   }//vpTest()


   /**
      A {@code main()} method for testing the {@code FrameBuffer} class.

      @param args  array of command-line arguments
   */
   public static void main(String[] args)
   {
      int w = 512;
      int h = 512;
      FrameBuffer fb = new FrameBuffer(w, h);
      fb.fbTest();  // fill the framebuffer with a test pattern
      fb.dumpFB2File("test01.ppm");

      fb.setViewport(64, 64, 192, 320);  // 192 by 320
      fb.clearVP( Color.red );
      for (int i = 0; i < 512; i++)
         fb.setPixelFB(128, i, Color.blue);
      for (int i = 0; i < 192; i++)
         fb.setPixelVP(i, i, Color.green);

      fb.dumpFB2File("test02.ppm");
      fb.dumpVP2File("test03.ppm");
      fb.dumpPixels2File(32, 256-64, 511-64, 255+64, "test04.ppm"); // 416 by 128

      fb.setViewport(80, 80, 160, 160);  // 160 by 160
      fb.vpTest();  // fill the viewport with a test pattern
      fb.dumpFB2File("test05.ppm");

      FrameBuffer fb2 = new FrameBuffer("test05.ppm");
      fb2.dumpFB2File("test06.ppm");

      fb.convertRed2FB().dumpFB2File("test07.ppm");
      fb.convertGreen2FB().dumpFB2File("test08.ppm");
      fb.convertBlue2FB().dumpFB2File("test09.ppm");
      fb.convertBlue2FB().convertVP2FB().dumpFB2File("test10.ppm");

      FrameBuffer fb3 = new FrameBuffer(600, 600);
      fb3.clearFB(Color.orange);
      fb3.setViewport(44, 44, "test05.ppm");
      fb3.dumpFB2File("test11.ppm");
      fb3.setViewport(86, 86, fb3.convertVP2FB());
      fb3.dumpFB2File("test12.ppm");
      fb3.dumpFB2File("test12.png", "png");
      fb3.dumpFB2File("test12.gif", "gif");
      fb3.dumpFB2File("test12.jpg", "jpg");
      fb3.dumpFB2File("test12.bmp", "bmp");

      // list the image file formats supported by the runtime
      for (String s : ImageIO.getWriterFormatNames()) System.out.println(s);

   }//main()
}
