
/*
    Scottie Richardson
    richar71@pnw.edu
    CS 455
    Homework 2
*/

import framebuffer.*;

import java.awt.Color;

public class Hw2 {
    public static void main(String[] args) {
        // Check for a file name on the command line.
        if (0 == args.length) {
            System.err.println("Usage: java Hw2 <PPM-file-name> [thickness]");
            System.exit(-1);
        }

        FrameBuffer fb = new FrameBuffer(args[0]);

        System.err.println("Processing framebuffer for " + args[0]);

        /****************************************/
        // Process the contents of the framebuffer.
        // Your solution goes here.
        int borderThickness = Integer.parseInt(args[1]);
        int shade = 0;
        Color fbb = fb.getPixelFB(0, 0);            // Framebuffer Background
        Color cbc = new Color(shade, shade, shade); // Current Border Color
        Color lbc = cbc;                            // Last Border Color
        int fbw = fb.getWidthFB();                  // Framebuffer Width
        int fbh = fb.getHeightFB();                 // Framebuffer Height

        for (int z = 0; z < borderThickness; z++) {
            for (int y = 0; y < fbh; ++y) {
                for (int x = 0; x < fbw; ++x) {
                    Color thisPixel = fb.getPixelFB(x, y);
                    if (thisPixel.equals(fbb) &&
                        (((x - 1) >= 0  && !fb.getPixelFB((x - 1), y).equals(fbb) && !fb.getPixelFB((x - 1), y).equals(cbc)) ||
                         ((y - 1) >= 0  && !fb.getPixelFB(x, (y - 1)).equals(fbb) && !fb.getPixelFB(x, (y - 1)).equals(cbc)) ||
                         ((x + 1) < fbw && !fb.getPixelFB((x + 1), y).equals(fbb) && !fb.getPixelFB((x + 1), y).equals(cbc)) ||
                         ((y + 1) < fbh && !fb.getPixelFB(x, (y + 1)).equals(fbb) && !fb.getPixelFB(x, (y + 1)).equals(cbc)) ||
                         ((x - 1) >= 0  && (y - 1) >= 0  && !fb.getPixelFB((x - 1), (y - 1)).equals(fbb) && !fb.getPixelFB((x - 1), (y - 1)).equals(cbc)) ||
                         ((x - 1) >= 0  && (y + 1) < fbh && !fb.getPixelFB((x - 1), (y + 1)).equals(fbb) && !fb.getPixelFB((x - 1), (y + 1)).equals(cbc)) ||
                         ((x + 1) < fbw && (y - 1) >= 0  && !fb.getPixelFB((x + 1), (y - 1)).equals(fbb) && !fb.getPixelFB((x + 1), (y - 1)).equals(cbc)) ||
                         ((x + 1) < fbw && (y + 1) < fbh && !fb.getPixelFB((x + 1), (y + 1)).equals(fbb) && !fb.getPixelFB((x + 1), (y + 1)).equals(cbc)) )) {
                                                fb.setPixelFB(x, y, cbc);
                    }
                }
            }
            shade++;
            if (shade > 255) {
                shade = 255;
            }
            lbc = cbc;
            cbc = new Color(shade, shade, shade);
        }

        /****************************************/
        // Save the resulting image in a file.
        String baseName = args[0].substring(0, args[0].indexOf("."));
        String savedFileName = String.format(baseName + "_bordered_" + "%03d" + ".ppm", borderThickness);
        fb.dumpFB2File(savedFileName);
        System.err.println("Saved " + savedFileName);
    }// main()
}