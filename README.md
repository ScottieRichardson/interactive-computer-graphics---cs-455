# Interactive Computer Graphics Assignments

This repository is a collection of the work that I completed in Interactive Computer Graphics or CS 455 at Purdue University Northwest.

It contains all of the completed assignments, assignment descriptions, and all files used within them.

I use Microsoft Visual Studio Code as a lightweight IDE and I use GIMP 2.10 as my graphic editor/viewer for the .ppm files and creating the .gif animations