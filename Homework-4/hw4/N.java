/*

*/

import renderer.scene.*;

/**


*/
public class N extends Model
{
   /**
      The letter N.
   */
   public N()
   {
      super();

      Vertex v0 = new Vertex(0.00, 0.00, 0.0);
      Vertex v1 = new Vertex(0.00, 1.00, 0.0);
      Vertex v2 = new Vertex(0.25, 1.00, 0.0);
      Vertex v3 = new Vertex(0.75, 0.5,  0.0);
      Vertex v4 = new Vertex(0.75, 1.0,  0.0);
      Vertex v5 = new Vertex(1.00, 1.0,  0.0);
      Vertex v6 = new Vertex(1.00, 0.0,  0.0);
      Vertex v7 = new Vertex(0.75, 0.0,  0.0);
      Vertex v8 = new Vertex(0.25, 0.5,  0.0);
      Vertex v9 = new Vertex(0.25, 0.0,  0.0);

      addVertex(v0, v1, v2, v3, v4, v5, v6, v7, v8, v9);

      // Create line segments.
      addLineSegment(0, 1);
      addLineSegment(1, 2);
      addLineSegment(2, 3);
      addLineSegment(3, 4);
      addLineSegment(4, 5);
      addLineSegment(5, 6);
      addLineSegment(6, 7);
      addLineSegment(7, 8);
      addLineSegment(8, 9);
      addLineSegment(9, 0);

      Vertex v10 = new Vertex(0.00, 0.00, -0.25);
      Vertex v11 = new Vertex(0.00, 1.00, -0.25);
      Vertex v12 = new Vertex(0.25, 1.00, -0.25);
      Vertex v13 = new Vertex(0.75, 0.5,  -0.25);
      Vertex v14 = new Vertex(0.75, 1.0,  -0.25);
      Vertex v15 = new Vertex(1.00, 1.0,  -0.25);
      Vertex v16 = new Vertex(1.00, 0.0,  -0.25);
      Vertex v17 = new Vertex(0.75, 0.0,  -0.25);
      Vertex v18 = new Vertex(0.25, 0.5,  -0.25);
      Vertex v19 = new Vertex(0.25, 0.0,  -0.25);

      addVertex(v10, v11, v12, v13, v14, v15, v16, v17, v18, v19);

      // Create line segments.
      addLineSegment(10, 11);
      addLineSegment(11, 12);
      addLineSegment(12, 13);
      addLineSegment(13, 14);
      addLineSegment(14, 15);
      addLineSegment(15, 16);
      addLineSegment(16, 17);
      addLineSegment(17, 18);
      addLineSegment(18, 19);
      addLineSegment(19, 10);



      addLineSegment(0, 10);
      addLineSegment(1, 11);
      addLineSegment(2, 12);
      addLineSegment(3, 13);
      addLineSegment(4, 14);
      addLineSegment(5, 15);
      addLineSegment(6, 16);
      addLineSegment(7, 17);
      addLineSegment(8, 18);
      addLineSegment(9, 19);
   }
}
