/*

*/

import renderer.scene.*;

/**


*/
public class P extends Model
{
   /**
      The letter P.
   */
   public P()
   {
      super();

      Vertex  v0 = new Vertex(0.00, 0.00, 0.0);
      Vertex  v1 = new Vertex(0.00, 1.00, 0.0);
      Vertex  v2 = new Vertex(0.75, 1.00, 0.0);
      Vertex  v3 = new Vertex(0.96, 0.8,  0.0);
      Vertex  v4 = new Vertex(0.96, 0.6,  0.0);
      Vertex  v5 = new Vertex(0.75, 0.4,  0.0);
      Vertex  v6 = new Vertex(0.25, 0.4,  0.0);
      Vertex  v7 = new Vertex(0.25, 0.0,  0.0);

      Vertex  v8 = new Vertex(0.25, 0.8,  0.0);
      Vertex  v9 = new Vertex(0.75, 0.8,  0.0);
      Vertex v10 = new Vertex(0.75, 0.6,  0.0);
      Vertex v11 = new Vertex(0.25, 0.6,  0.0);

      addVertex(v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11);

      // Create line segments.
      addLineSegment(0, 1);
      addLineSegment(1, 2);
      addLineSegment(2, 3);
      addLineSegment(3, 4);
      addLineSegment(4, 5);
      addLineSegment(5, 6);
      addLineSegment(6, 7);
      addLineSegment(7, 0);

      addLineSegment(8, 9);
      addLineSegment(9, 10);
      addLineSegment(10, 11);
      addLineSegment(11, 8);

      Vertex v12 = new Vertex(0.00, 0.00, -0.25);
      Vertex v13 = new Vertex(0.00, 1.00, -0.25);
      Vertex v14 = new Vertex(0.75, 1.00, -0.25);
      Vertex v15 = new Vertex(0.96, 0.8,  -0.25);
      Vertex v16 = new Vertex(0.96, 0.6,  -0.25);
      Vertex v17 = new Vertex(0.75, 0.4,  -0.25);
      Vertex v18 = new Vertex(0.25, 0.4,  -0.25);
      Vertex v19 = new Vertex(0.25, 0.0,  -0.25);

      Vertex v20 = new Vertex(0.25, 0.8,  -0.25);
      Vertex v21 = new Vertex(0.75, 0.8,  -0.25);
      Vertex v22 = new Vertex(0.75, 0.6,  -0.25);
      Vertex v23 = new Vertex(0.25, 0.6,  -0.25);

      addVertex(v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23);

      // Create line segments.
      addLineSegment(12, 13);
      addLineSegment(13, 14);
      addLineSegment(14, 15);
      addLineSegment(15, 16);
      addLineSegment(16, 17);
      addLineSegment(17, 18);
      addLineSegment(18, 19);
      addLineSegment(19, 12);

      addLineSegment(20, 21);
      addLineSegment(21, 22);
      addLineSegment(22, 23);
      addLineSegment(23, 20);


      addLineSegment(0, 12);
      addLineSegment(1, 13);
      addLineSegment(2, 14);
      addLineSegment(3, 15);
      addLineSegment(4, 16);
      addLineSegment(5, 17);
      addLineSegment(6, 18);
      addLineSegment(7, 19);
      addLineSegment(8, 20);
      addLineSegment(9, 21);
      addLineSegment(10, 22);
      addLineSegment(11, 23);
   }
}
