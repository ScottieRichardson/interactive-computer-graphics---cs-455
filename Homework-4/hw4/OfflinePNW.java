 /*
      Scottie Richardson
      richar71@pnw.edu
      CS 455
      Homework 4
*/

import renderer.scene.*;
import renderer.models.*;
import renderer.pipeline.*;
import renderer.framebuffer.*;

import java.awt.Color;

/**

*/
public class OfflinePNW
{
   public static void main(String[] args)
   {
      // Create the Scene object that we shall render.
      final Scene scene = new Scene();

      // Create a set of x and y axes.
      Model axes = new Axes2D(-2, +2, -2, +4, 8, 12);
      axes.setColor( Color.red );
      Position axes_p = new Position( axes );
      scene.addPosition( axes_p );
      // Push the axes away from where the camera is.
      axes_p.translate(0, 0, -3);

      // Add the letters to the Scene.
      scene.addPosition(new Position( new P() ),
                        new Position( new N() ),
                        new Position( new W() ));

      // Give the letters random colors.
      for (Position p : scene.positionList)
      {
         p.model.setRandomColor();
      }

      // Create a FrameBuffer to render our scene into.
      int width  = 512;
      int height = 512;
      FrameBuffer fb = new FrameBuffer(width, height);

      // Create the animation frames.
      for (int i = 0; i < 360; i++)
      {
         // Push the letters away from the camera.
         scene.positionList.get(1).matrix2Identity();   // P
         scene.positionList.get(1).translate(0, 0, -3); // P
         scene.positionList.get(2).matrix2Identity();   // N
         scene.positionList.get(2).translate(0, 0, -3); // N
         scene.positionList.get(3).matrix2Identity();   // W
         scene.positionList.get(3).translate(0, 0, -3); // W

         // do P





         // do N





         // do W





         // Render again.
         fb.clearFB(Color.black);
         Pipeline.render(scene, fb);
         fb.dumpFB2File(String.format("PPM_PNW_Frame%03d.ppm", i));
      }
   }
}
