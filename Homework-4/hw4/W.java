/*

*/

import renderer.scene.*;

/**


*/
public class W extends Model
{
   /**
      The letter W.
   */
   public W()
   {
      super();

      Vertex  v0 = new Vertex(0.0, 1.00, 0.0);
      Vertex  v1 = new Vertex(0.2, 1.00, 0.0);
      Vertex  v2 = new Vertex(0.3, 0.45, 0.0);
      Vertex  v3 = new Vertex(0.4, 1.00, 0.0);
      Vertex  v4 = new Vertex(0.6, 1.00, 0.0);
      Vertex  v5 = new Vertex(0.7, 0.45, 0.0);
      Vertex  v6 = new Vertex(0.8, 1.00, 0.0);
      Vertex  v7 = new Vertex(1.0, 1.00, 0.0);
      Vertex  v8 = new Vertex(0.8, 0.00, 0.0);
      Vertex  v9 = new Vertex(0.6, 0.00, 0.0);
      Vertex v10 = new Vertex(0.5, 0.55, 0.0);
      Vertex v11 = new Vertex(0.4, 0.00, 0.0);
      Vertex v12 = new Vertex(0.2, 0.00, 0.0);

      addVertex(v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12);

      // Create line segments.
      addLineSegment(0, 1);
      addLineSegment(1, 2);
      addLineSegment(2, 3);
      addLineSegment(3, 4);
      addLineSegment(4, 5);
      addLineSegment(5, 6);
      addLineSegment(6, 7);
      addLineSegment(7, 8);
      addLineSegment(8, 9);
      addLineSegment(9, 10);
      addLineSegment(10, 11);
      addLineSegment(11, 12);
      addLineSegment(12, 0);


      Vertex v13 = new Vertex(0.0, 1.00, -0.25);
      Vertex v14 = new Vertex(0.2, 1.00, -0.25);
      Vertex v15 = new Vertex(0.3, 0.45, -0.25);
      Vertex v16 = new Vertex(0.4, 1.00, -0.25);
      Vertex v17 = new Vertex(0.6, 1.00, -0.25);
      Vertex v18 = new Vertex(0.7, 0.45, -0.25);
      Vertex v19 = new Vertex(0.8, 1.00, -0.25);
      Vertex v20 = new Vertex(1.0, 1.00, -0.25);
      Vertex v21 = new Vertex(0.8, 0.00, -0.25);
      Vertex v22 = new Vertex(0.6, 0.00, -0.25);
      Vertex v23 = new Vertex(0.5, 0.55, -0.25);
      Vertex v24 = new Vertex(0.4, 0.00, -0.25);
      Vertex v25 = new Vertex(0.2, 0.00, -0.25);

      addVertex(v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25);

      // Create line segments.
      addLineSegment(13, 14);
      addLineSegment(14, 15);
      addLineSegment(15, 16);
      addLineSegment(16, 17);
      addLineSegment(17, 18);
      addLineSegment(18, 19);
      addLineSegment(19, 20);
      addLineSegment(20, 21);
      addLineSegment(21, 22);
      addLineSegment(22, 23);
      addLineSegment(23, 24);
      addLineSegment(24, 25);
      addLineSegment(25, 13);


      addLineSegment(0, 13);
      addLineSegment(1, 14);
      addLineSegment(2, 15);
      addLineSegment(3, 16);
      addLineSegment(4, 17);
      addLineSegment(5, 18);
      addLineSegment(6, 19);
      addLineSegment(7, 20);
      addLineSegment(8, 21);
      addLineSegment(9, 22);
      addLineSegment(10, 23);
      addLineSegment(11, 24);
      addLineSegment(12, 25);
   }
}
