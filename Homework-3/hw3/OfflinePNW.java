
/*
    Scottie Richardson
    richar71@pnw.edu
    CS 455
    Homework 3
*/
import renderer.scene.*;
import renderer.models.*;
import renderer.pipeline.*;
import renderer.framebuffer.*;

import java.io.File;
import java.awt.Color;

public class OfflinePNW {

    public static Vertex rotateAboutY (Vertex v, double step, int i) {
        if (i < 50) {
            // Rotate left
            v.x = (v.z * Math.sin(step) + v.x * Math.cos(step));
            v.z = (v.z * Math.cos(step) - v.x * Math.sin(step));
        } else if (i >= 50 && i < 150) {
            // Rotate right
            v.x = (v.z * Math.sin(-step) + v.x * Math.cos(-step));
            v.z = (v.z * Math.cos(-step) - v.x * Math.sin(-step));
        } else if (i >= 150 && i < 200) {
            // Rotate left
            v.x = (v.z * Math.sin(step) + v.x * Math.cos(step));
            v.z = (v.z * Math.cos(step) - v.x * Math.sin(step));
        } else if (i >= 200 && i < 250) {
            // Rotate left
            v.x = (v.z * Math.sin(-step) + v.x * Math.cos(-step));
            v.z = (v.z * Math.cos(-step) - v.x * Math.sin(-step));
        }
        return v;
    }

    public static Vertex verticalMove (Vertex v, double step, int i) {
        // Vertical movment
        if(i < 50) {
            v.y += step; // Move up
        } else if(i >= 50 && i < 150) {
            v.y -= step; // Move down
        } else if(i >= 150 && i < 200) {
            v.y += step; // Move up
        } else if(i >= 200 && i < 250) {
            v.y -= step; // Move down
        }
        return v;
    }

    public static Vertex rotateAboutZ (Vertex v, double step, int i) {
        // Spin
        if(i < 100) {
            // Spin left
            v.x = (v.x * Math.cos(step) - v.y * Math.sin(step));
            v.y = (v.x * Math.sin(step) + v.y * Math.cos(step));
        } else if(i >= 100 && i < 200) {
            // Spin right
            v.x = (v.x * Math.cos(-step) - v.y * Math.sin(-step));
            v.y = (v.x * Math.sin(-step) + v.y * Math.cos(-step));
        } else if(i >= 200 && i < 250) {
            // Spin left
            v.x = (v.x * Math.cos(step) - v.y * Math.sin(step));
            v.y = (v.x * Math.sin(step) + v.y * Math.cos(step));
        }
        return v;
    }

    public static void main(String[] args) {
        // Create several Model objects.
        Model p = new P();
        Model n = new N();
        Model w = new W();

        // Create the Scene object that we shall render
        Scene scene = new Scene();

        // Add Models to the Scene.
        scene.addModel(p, n, w);

        // Push the models away from where the camera is.
        for (Model m : scene.modelList)
            for (Vertex v : m.vertexList) {
                v.z -= 15;
            }

        // Give the models random colors.
        for (Model m : scene.modelList) {
            m.setColorRandom();
        }

        // Create a framebuffer to render our scene into.
        int vp_width = 1024;
        int vp_height = 1024;
        FrameBuffer fb = new FrameBuffer(vp_width, vp_height);
        // Give the framebuffer a background color.
        fb.clearFB(Color.black);

        // Render our scene into the frame buffer.
        Pipeline.render(scene, fb);
        // Save the resulting image in a file.
        fb.dumpFB2File(String.format("PPM_Frame%03d.ppm", 0));

        // Step variables
        double pStep = 0.10;
        double nStep = 0.15;
        double wStep = 0.20;
        double rotationStep = 0.01;

        for (int i = 1; i <= 250; i++) {
            // update the scene
            for (Vertex v : scene.modelList.get(0).vertexList) {
                v = verticalMove(v, pStep, i);
                v = rotateAboutY(v, rotationStep, i);
                v = rotateAboutZ(v, rotationStep, i);
            }
            for (Vertex v : scene.modelList.get(1).vertexList) {
                v = verticalMove(v, nStep, i);
                v = rotateAboutY(v, rotationStep, i);
                v = rotateAboutZ(v, rotationStep, i);
            }
            for (Vertex v : scene.modelList.get(2).vertexList) {
                v = verticalMove(v, wStep, i);
                v = rotateAboutY(v, rotationStep, i);
                v = rotateAboutZ(v, rotationStep, i);
            }

            // Render again.
            fb.clearFB(Color.black);
            Pipeline.render(scene, fb);
            fb.dumpFB2File(String.format("PPM_Frame%03d.ppm", i));
        }
    }
}