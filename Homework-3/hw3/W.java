
/*
    Scottie Richardson
    richar71@pnw.edu
    CS 455
    Homework 3
*/

import renderer.scene.*;

public class W extends Model {
    /**
     * The letter W.
     */
    public W() {
        super();

        int n = 26; // number of vertices

        Vertex[] v = new Vertex[n];

        // shifts the model to prevent overlap in scene
        int shift = 4;

        // Create vertices.
         v[0] = new Vertex((0.00 + shift), 5, 0);
         v[1] = new Vertex((0.50 + shift), 5, 0);
         v[2] = new Vertex((1.00 + shift), 2, 0);
         v[3] = new Vertex((1.25 + shift), 5, 0);
         v[4] = new Vertex((1.75 + shift), 5, 0);
         v[5] = new Vertex((2.00 + shift), 2, 0);
         v[6] = new Vertex((2.50 + shift), 5, 0);
         v[7] = new Vertex((3.00 + shift), 5, 0);
         v[8] = new Vertex((2.50 + shift), 0, 0);
         v[9] = new Vertex((2.00 + shift), 0, 0);
        v[10] = new Vertex((1.50 + shift), 3, 0);
        v[11] = new Vertex((1.00 + shift), 0, 0);
        v[12] = new Vertex((0.50 + shift), 0, 0);
        v[13] = new Vertex((0.00 + shift), 5, 1);
        v[14] = new Vertex((0.50 + shift), 5, 1);
        v[15] = new Vertex((1.00 + shift), 2, 1);
        v[16] = new Vertex((1.25 + shift), 5, 1);
        v[17] = new Vertex((1.75 + shift), 5, 1);
        v[18] = new Vertex((2.00 + shift), 2, 1);
        v[19] = new Vertex((2.50 + shift), 5, 1);
        v[20] = new Vertex((3.00 + shift), 5, 1);
        v[21] = new Vertex((2.50 + shift), 0, 1);
        v[22] = new Vertex((2.00 + shift), 0, 1);
        v[23] = new Vertex((1.50 + shift), 3, 1);
        v[24] = new Vertex((1.00 + shift), 0, 1);
        v[25] = new Vertex((0.50 + shift), 0, 1);

        // Create line segments.
        this.addLineSegment(v[0], v[12]);
        this.addLineSegment(v[0], v[1]);
        this.addLineSegment(v[0], v[13]);
        this.addLineSegment(v[1], v[2]);
        this.addLineSegment(v[1], v[14]);
        this.addLineSegment(v[2], v[3]);
        this.addLineSegment(v[2], v[15]);
        this.addLineSegment(v[3], v[4]);
        this.addLineSegment(v[3], v[16]);
        this.addLineSegment(v[4], v[5]);
        this.addLineSegment(v[4], v[17]);
        this.addLineSegment(v[5], v[6]);
        this.addLineSegment(v[5], v[18]);
        this.addLineSegment(v[6], v[7]);
        this.addLineSegment(v[6], v[19]);
        this.addLineSegment(v[7], v[8]);
        this.addLineSegment(v[7], v[20]);
        this.addLineSegment(v[8], v[9]);
        this.addLineSegment(v[8], v[21]);
        this.addLineSegment(v[9], v[10]);
        this.addLineSegment(v[9], v[22]);
        this.addLineSegment(v[10], v[11]);
        this.addLineSegment(v[10], v[23]);
        this.addLineSegment(v[11], v[12]);
        this.addLineSegment(v[11], v[24]);
        this.addLineSegment(v[12], v[25]);
        this.addLineSegment(v[13], v[25]);
        this.addLineSegment(v[13], v[14]);
        this.addLineSegment(v[14], v[15]);
        this.addLineSegment(v[15], v[16]);
        this.addLineSegment(v[16], v[17]);
        this.addLineSegment(v[17], v[18]);
        this.addLineSegment(v[18], v[19]);
        this.addLineSegment(v[19], v[20]);
        this.addLineSegment(v[20], v[21]);
        this.addLineSegment(v[21], v[22]);
        this.addLineSegment(v[22], v[23]);
        this.addLineSegment(v[23], v[24]);
        this.addLineSegment(v[24], v[25]);

    }
}
