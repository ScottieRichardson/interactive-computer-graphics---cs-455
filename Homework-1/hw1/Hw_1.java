
/*
    Scottie Richardson
    richar71@pnw.edu
    CS 455
    Homework 1
*/

import framebuffer.*;

import java.awt.Color;

public class Hw_1 {
    public static void main(String[] args) {
        // Check for a file name on the command line.
        if (0 == args.length) {
            System.err.println("Usage: java Hw_1 <PPM-file-name>");
            System.exit(-1);
        }

        // This framebuffer holds the image that will be embedded
        // within a viewport of our larger framebuffer.
        FrameBuffer fbEmbedded = new FrameBuffer(args[0]);

        /******************************************/

        // Your code goes here.
        // Create a framebuffer. Fill it with the checkerboard pattern.
        FrameBuffer fin = new FrameBuffer(1000, 600);
        int square = 100;
        boolean switchColor = true;

        for (int y = 0; y < fin.getHeightFB(); y += square) {
            for (int x = 0; x < fin.getWidthFB(); x += square) {
                fin.setViewport(x, y, square, square);
                if (switchColor) {
                    fin.clearVP(new Color(0xFFBD60));
                } else {
                    fin.clearVP(new Color(0xC0380E));
                }
                switchColor = !switchColor;
            }
            switchColor = !switchColor;
        }

        // Create a viewport to hold the given PPM file. Put the PPM file in the
        // viewport.
        int inputX = 75;
        int inputY = 125;
        fin.setViewport(inputX, inputY, fbEmbedded);

        // Create another viewport and fill it with a flipped copy of the PPM file.
        int flipX = fbEmbedded.getWidthFB();
        int flipY = fbEmbedded.getHeightFB();
        FrameBuffer flippedInput = new FrameBuffer(flipX, flipY);

        for (int y = 0; y < flipY; y++) {
            for (int x = 0; x < flipX; x++) {
                flippedInput.setPixelFB(x, y, fbEmbedded.getPixelFB(flipX - 1 - x, y));
            }
        }

        fin.setViewport((inputX + flipX), inputY, flippedInput);

        // Create another viewport and fill it with the striped pattern.
        int paint = 0;
        int shift = 0;
        int stripeWidth = 300;
        int stripeHeight = 120;
        int extendedWidth = (stripeWidth * 2);
        Color[] colors = new Color[3];
        Color[] stripedArray = new Color[extendedWidth];
        FrameBuffer striped = new FrameBuffer(stripeWidth, stripeHeight);

        colors[0] = new Color(0xF15F74);
        colors[1] = new Color(0x98CB4A);
        colors[2] = new Color(0x5481E6);

        for (int x = 0; x < stripedArray.length; x += 30) {
            for (int z = 0; z < 30; z++) {
                if ((x + z) < stripedArray.length) {
                    stripedArray[(x + z)] = colors[paint];
                }
            }
            paint++;
            if (paint == colors.length) {
                paint = 0;
            }
        }

        for (int y = 0; y < stripeHeight; y++) {
            for (int x = 0; x < stripeWidth; x++) {
                for (int z = 0; z < 30; z++) {
                    if ((x + z) < 300) {
                        striped.setPixelFB((x + z), y, stripedArray[(x + z) + shift]);
                    }
                }
            }
            shift++;
        }

        fin.setViewport(610, 420, striped);

        // Create another viewport that covers the selected region of the framebuffer.
        fin.setViewport(500, 200, 200, 300);

        // Convert this viewport into a new framebuffer (fb2).
        FrameBuffer newBuffer = fin.convertVP2FB();

        // Create another viewport to hold a copy of the selected region.
        fin.setViewport(725, 25, 250, 350);

        // Give this viewport a grayish background color.
        fin.clearVP(new Color(0xC0C0C0));

        // Create another viewport inside the last one.
        // Copy the framebuffer fb2 into this last viewport.
        fin.setViewport(750, 50, newBuffer);

        /******************************************/
        // Save the resulting image in a file.
        String savedFileName = "Hw_1.ppm";
        fin.dumpFB2File(savedFileName);
        System.err.println("Saved " + savedFileName);
    }
}
